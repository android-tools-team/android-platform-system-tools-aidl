% AIDL(1)
% The Android Open Source Project

# NAME

aidl-cpp - C++ bindings generator of AIDL interfaces

# SYNOPSIS

**aidl-cpp** _INPUT_FILE_ _HEADER_DIR_ _OUTPUT_FILE_

# OPTIONS

-I\<DIR\>
: Search path for import statements.

-d\<FILE\>
: Generate dependency file.

-ninja
: Generate dependency file in a format ninja understands.

_INPUT_FILE_
: An aidl interface file.

_HEADER_DIR_
: An empty directory to put generated headers.

_OUTPUT_FILE_
: Path to write generated .cpp code

# SEE ALSO

https://developer.android.com/guide/components/aidl.html