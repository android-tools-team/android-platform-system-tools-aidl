NAME = libaidl-common
SOURCES = \
          aidl.cpp \
          aidl_apicheck.cpp \
          aidl_language.cpp \
          aidl_typenames.cpp \
          aidl_to_cpp.cpp \
          aidl_to_java.cpp \
          aidl_to_ndk.cpp \
          ast_cpp.cpp \
          ast_java.cpp \
          code_writer.cpp \
          generate_cpp.cpp \
          aidl_to_cpp_common.cpp \
          generate_ndk.cpp \
          generate_java.cpp \
          generate_java_binder.cpp \
          generate_aidl_mappings.cpp \
          import_resolver.cpp \
          line_reader.cpp \
          io_delegate.cpp \
          options.cpp \
          type_cpp.cpp \
          type_java.cpp \
          type_namespace.cpp \

SOURCES += aidl_language_l.cpp aidl_language_y.cpp
CXXFLAGS += -std=c++17
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0 \
           -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase -lcutils

$(NAME).so: $(SOURCES)
	$(CXX) $^ -o $(NAME).so.0 $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
	ln -s $(NAME).so.0 $(NAME).so

aidl_language_l.cpp: aidl_language_l.ll aidl_language_y.cpp
	flex -o $@ $<
