NAME = aidl
SOURCES = main.cpp
CXXFLAGS += -std=c++17
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -laidl-common \
           -L. -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase -latomic

$(NAME): $(SOURCES)
	$(CXX) $^ -o $(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
